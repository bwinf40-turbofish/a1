use std::cmp::Ordering;
use std::env;
use std::fs;
use std::process;

type Field = Option<(char, u8)>;

fn main() {
    const USAGE: &str = "Benutzung: a1 <eingabedatei>";

    let mut args = env::args();
    args.next();

    // Speichere den Dateipfad
    let path = args.next().unwrap_or_else(|| {
        // Beende das Programm, wenn kein Dateipfad angegeben wurde
        eprintln!("{}", USAGE);
        process::exit(1);
    });

    // Beende das Programm, wenn zu viele Argumente angegeben wurden
    if args.next().is_some() {
        eprintln!("{}", USAGE);
        process::exit(1);
    }

    // Lese die gegebene Datei ein
    let file = fs::read_to_string(path).expect("Die Datei konnte nicht geöffnet werden");

    // Wandle die Eingabedatei in eine Liste an Feldern um
    let mut lines = file.lines();

    let num_fields = (lines.next().unwrap().as_bytes()[2] - 64) as usize;

    let mut fields: Vec<Field> = vec![None; num_fields];

    lines.next();
    for line in lines {
        let mut words = line.split_whitespace();

        let car: char = words.next().unwrap().parse().unwrap();
        let pos: usize = words.next().unwrap().parse().unwrap();

        // Jedes Auto nimmt zwei Felder ein
        fields[pos] = Some((car, 1));
        fields[pos + 1] = Some((car, 2));
    }

    // Gib die Lösung aus
    for i in 0..num_fields {
        println!("{}", solve(&fields, i));
    }
}

fn solve(fields: &[Field], pos: usize) -> String {
    let curr = String::from((pos as u8 + 65) as char) + ":";
    if fields[pos].is_none() {
        return curr;
    }
    let l = solve_l(fields, pos);
    let r = solve_r(fields, pos);
    curr + " "
        + &match (l, r) {
            (None, None) => String::from("Nicht möglich"),
            (Some((.., string)), None) | (None, Some((.., string))) => string,
            (Some((moves_l, moved_cars_l, string_l)), Some((moves_r, moved_cars_r, string_r))) => {
                match moves_l.cmp(&moves_r) {
                    Ordering::Less => string_l,
                    Ordering::Greater => string_r,
                    Ordering::Equal => match moved_cars_l.cmp(&moved_cars_r) {
                        Ordering::Less | Ordering::Equal => string_l,
                        Ordering::Greater => string_r,
                    },
                }
            }
        }
}

fn solve_r(fields: &[Field], pos: usize) -> Option<(u8, u8, String)> {
    if pos >= fields.len() - 2 {
        return None;
    }
    let (car, moves) = fields[pos].unwrap();
    let string = format!("{} {} rechts", car, moves);
    match fields[pos + 2] {
        None => Some((moves, 1, string)),
        _ => solve_r(fields, pos + 2)
            .map(|(m, moved_cars, s)| (moves + m, moved_cars + 1, s + ", " + &string)),
    }
}

fn solve_l(fields: &[Field], pos: usize) -> Option<(u8, u8, String)> {
    if pos < 2 {
        return None;
    }
    let (car, moves) = fields[pos].unwrap();
    let moves = 3 - moves;
    let string = format!("{} {} links", car, moves);
    match fields[pos - 2] {
        None => Some((moves, 1, string)),
        _ => solve_l(fields, pos - 2)
            .map(|(m, moved_cars, s)| (moves + m, moved_cars + 1, s + ", " + &string)),
    }
}
