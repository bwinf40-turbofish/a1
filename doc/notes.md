# Lösungsidee

Ziel der Aufgabe ist es, für jedes Auto, das sich in der gänzlich vollen ersten Parkreihe befindet, die Kombination an Zügen der verschiebbaren Autos zu finden, die von der gegeben Ausgangssituation aus dazu führt, dass der Platz vor dem parkenden Auto frei wird, sodass dieses ausparken kann.
Wenn es mehrere solcher Kombinationen gibt, soll die gewählt werden, bei der die wenigsten Autos verschoben werden.

Allgemein lässt sich feststellen, dass es niemals sinnvoll ist, ein Auto mehr als zwei Felder zu verschieben.
Außerdem fallen alle Kombinationen an Zügen weg, bei denen Autos sowohl nach rechts, als auch nach links verschoben werden, da sich diese Kombinationen immer soweit vereinfachen lassen, dass entweder nur noch Züge nach rechts bzw. links vorhanden sind.

Wenn ein parkendes Auto nicht von einem verschiebbaren Auto blockiert ist, dann ist die korrekte Kombination an Zügen, kein Auto zu verschieben, da das parkende Auto einfach direkt ausparken kann.
Wenn dem allerdings nicht so ist, dann gilt es, das blockierende Auto zu verschieben, sodass der Platz vor dem parkenden Auto frei wird.
Hierbei gibt es zwei Möglichkeiten: Man kann es nach rechts oder links schieben.
Diese zwei Möglichkeiten sind nacheinander durchzurechnen und dann miteinander auf Basis der Anzahl an bewegten Autos zu vergleichen.
Dies entspricht dem in der Aufgabe vorgegebenen Kriterium.
Wenn anhand dieses Kriteriums nicht klar zwischen den beiden Lösungen entschieden werden kann, werden außerdem die Summen der Längen jedes Zuges (um wie viele Plätze ein Auto in einem Zug verschoben wird) verglichen und wenn auch diese übereinstimmen ist die Kombination als korrekt zu betrachten, bei der alle Autos nach links verschoben werden, da das Wort "links" weniger Buchstaben als das Wort "rechts" besitzt.
Diese absichernden Kriterien erschienen uns als sinnvoll.
Durchgerechnet werden die zwei zu vergleichenden Möglichkeiten wie folgt.
Wenn das zu verschiebende Auto einfach nach rechts bzw. links geschoben werden kann, sodass der Platz vor dem parkenden Auto frei wird, dann hat man eine Lösung gefunden.
Wenn dem nicht so ist und das zu verschiebende Auto von einem anderen verschiebbaren Auto blockiert wird, dann gilt es, das blockierende Auto weg von dem verschiebbaren Auto zu schieben, sodass man das verschiebbare Auto dahin bewegen kann, wo man es anfangs hinbewegen wollte.
Wenn das blockierende Auto selbst blockiert wird, dann gilt es, zuerst das blockierende Auto des blockierenden Autos weg zu bewegen und so weiter.
Hierbei kann es auch sein, dass ein verschiebbares Auto gar nicht nach rechts bzw. links verschoben werden kann, weil es von der Wand (im gegebenen Bild des Schiebeparkplatzes ist diese grün gekennzeichnet und bildet den Grund dafür, dass die in der ersten Parkreihe stehenden Autos nicht einfach nach vorne ausparken können) blockiert wird.
Wenn dem so ist, dann führt das dazu, dass das Auto, das blockiert wird, auch nicht verschoben werden kann und demnach das Auto, das durch das blockierte Auto blockiert wird auch nicht und so weiter, weshalb es dann auch nicht möglich ist, das ursprünglich zu verschiebende Auto in die Richtung zu verschieben, in der das Auto durch die Wand blockiert wird.
Wenn dies für beide zu berechnenden Anfangsmöglichkeiten gilt, dann gibt es generell keine Möglichkeit, dass das Auto in der ersten Reihe ausparken kann.
Dieser Grenzfall taucht zwar nicht in den Beispielen auf, erschien uns aber relevant, weshalb wir auch diesen Fall abgedeckt haben, indem wir in der Ausgabe des Programmes explizit darauf hinweisen.

# Umsetzung

```
A B C D E F G
0 1 2 3 4 5 6
- - H H - I I
    1 2   1 2
```

Die erste Hälfte eines Autos bekommt außerdem die Zahl 1 und die zweite Hälfte die Zahl 2 zugewiesen (erweiterbar, wenn es längere Autos gäbe).

Hier wird kurz erläutert, wie die Lösungsidee im Programm tatsächlich umgesetzt wurde. Hier können auch Implementierungsdetails erwähnt werden.

# Beispiele

Genügend Beispiele einbinden! Die Beispiele von der BwInf-Webseite sollten hier diskutiert werden, aber auch eigene Beispiele sind sehr gut --- besonders wenn sie Spezialfälle abdecken. Aber bitte nicht 30 Seiten Programmausgabe hier einfügen!

# Quellcode

Unwichtige Teile des Programms sollen hier nicht abgedruckt werden. Dieser Teil sollte nicht mehr als 2–3 Seiten umfassen, maximal 10.
